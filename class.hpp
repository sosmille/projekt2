#ifndef class_hpp
#define class_hpp

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

class Point
{

private:
    int x, y, z;

public:
    //constructors
    Point();
    Point(int x1, int y1, int z1);

    //destructor
    ~Point();

    //functions
    int get_x() const;
    int get_y() const;
    int get_z() const;
    void display_node();

    //operators
    Point operator+(const Point & p) const;
    Point & operator+=(Point p);
    Point operator-(const Point & p) const;
    Point & operator-=(Point p);
    Point operator*(const Point & p) const;
    Point & operator*=(Point p);

    bool operator==(const Point & p) const;
    bool operator!=(const Point & p) const;

    //friends
    friend std::ostream & operator <<( std::ostream & os, const Point & p );

};


struct node
{
    Point p;
    node *next;
};


class list
{

private:
    node *head; // first element
    node *tail; //last element

public:
    //constructors
    list();
    list(node* head1);

    //destructors
    ~list();

    //functions
    void add_node(Point p1);
    void delete_node(int n);
    void display_list();
    int elements_list() const;

    node * get_head() const ;
    node * get_tail() const ;

    //operators
    list operator+(const list & l) ;
    list & operator+=(list l);
    Point operator[](int n) const;

};

//other stuff
void add_point (Point &p);
void add_list(list &l);
void check_node(node *p);

void caption(); //function print start
void check_how_it_works(); //function shows how program works


#endif








































