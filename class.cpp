#include "class.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

// *****CLASS POINT *****

//constructors
Point::Point()
{
    x = 0;
    y = 0;
    z = 0;
}

Point::Point(int x1, int y1, int z1) : x(x1), y(y1), z(z1)
{
}

//destructor
Point::~Point()
{
}

//operators
Point Point::operator+(const Point & p1) const
{
    return {this->x + p1.x, this->y + p1.y, this->z + p1.z};
}

Point& Point::operator+=(Point p1)
{
    this->x += p1.x;
    this->y += p1.y;
    this->z += p1.z;
    return *this;
}

Point Point::operator-(const Point & p1) const
{
    return {this->x - p1.x, this->y - p1.y, this->z - p1.z};
}

Point& Point::operator-=(Point p1)
{
    this->x -= p1.x;
    this->y -= p1.y;
    this->z -= p1.z;
    return *this;;
}

Point Point::operator*(const Point & p1) const
{
    return {this->x * p1.x, this->y * p1.y, this->z * p1.z};
}

Point& Point::operator*=(Point p1)
{
    this->x *= p1.x;
    this->y *= p1.y;
    this->z *= p1.z;
    return *this;
}

bool Point::operator==(const Point &p1) const
{
    return (this->x == p1.x && this->y==p1.y && this->z==p1.z);
}

bool Point::operator!=(const Point &p1) const
{
    return !(*this == p1);
}

//functions
int Point::get_x() const
{
    return this->x;
}

int Point::get_y() const
{
    return this->y;
}

int Point::get_z() const
{
    return this->z;
}

void Point::display_node()
{
    cout << "(" << this->x << ", " << this->y << ", " << this->z << ")"<<endl;
}

//friends
std::ostream & operator <<( std::ostream & os, const Point & p1 )
{
    return os << "(" << p1.x << ", " << p1.y << ", " << p1.z << ")"<<endl;
}



//******CLASS LIST*****

//constructors
list::list()
{
    head=NULL;
    tail=NULL;
}

list::list(node* head1)
{
    head = head1;
    tail = head1;
};

//destructor
list::~list()
{
}

//functions
void list::add_node(Point p1) //function adds node to list
{
    node *temp = new node;
    check_node(temp);
    temp->p = p1;
    temp->next = NULL;

    if(head == NULL)
    {
        head = temp;
        tail = temp;
    }
    else
    {
        tail->next=temp;
        tail=temp;
    }
}

void list::display_list() //function displays all points in list
{
    node *temp;
    temp = head;
    int i=1;
    while(temp!=NULL)
    {
        cout <<i<<".    "<< temp->p;
        temp=temp->next;
        i++;
    }
}

void list::delete_node(int n) //function deletes point nr. n from the list, if there is no poni n, does nothing
{
    node *temp = head;
    node *del;
    n--;
    int num = 1;
    if(n == 0)
    {
        head = head->next;
        delete temp;
    }
    else
    {
        while (temp != NULL && num != n)
        {
            num += 1;
            temp = temp->next;
        }
        if (num == n && temp!=NULL)
        {
            del = temp->next;
            temp->next = temp->next->next;
            delete del;
        }
        else
        {
            cout << "We don't have " << n+1 <<" points. Try later alligator :>" <<endl;
        }
    }
}

int list::elements_list () const //function returns number of points in list
{
    node *temp;
    temp = head;

    int i=1;
    if(NULL==temp)
        return 0;
    while(NULL!=temp->next)
    {
        i++;
        temp=temp->next;
    }
    return i;
}

node * list::get_head() const //function returns pointer on head
{
    return this->head;
}

node * list::get_tail() const //function returns pointer on tail
{
    return this->tail;
}


//operators
list list::operator+(const list & l)
{
    list sum=list();
    node * temp;
    temp = this->get_head();

    while(temp != NULL)
    {
        sum.add_node(temp->p);
        temp=temp->next;

    }
    temp = l.get_head();

    while(temp != NULL)
    {
        sum.add_node(temp->p);
        temp=temp->next;
    }
    return sum;
}

list & list::operator+=(list l)
{
    node *temp;
    temp = l.get_head();
    while(NULL != temp)
    {
        this->add_node(temp->p);
        temp = temp->next;
    }
    return *this;
}

Point list:: operator[](int n) const
{
    node *temp;
    temp=head;
    n--;
    int i=1;
    int numberOfElements=this->elements_list();
    if(n<numberOfElements)
    {
        if(n==0)
            return head->p;
        else
        {
            while((NULL != temp )&& (n!=i))
            {
                i++;
                temp=temp->next;
            }
            return temp->next->p;
        }
    }
    else
    {
        cout << "There are only "<< numberOfElements <<" points in this list. Function returns (0, 0, 0)" << endl;
        return Point();
    }
}


//other stuff
void add_point (Point &p) //function gives opportunity to user, to enter data yourself
{
    int x,y,z;
    cout << "Enter coordinates." << endl;
        cout << "x:";
        cin >>x;
        cout << "y:";
        cin >> y;
        cout << "z:";
        cin >> z;
        p=Point(x,y,z);
}


void add_list (list &l) //function gives opportunity to user, to enter data yourself
{
    int n,x,y,z;
    cout << "How many points want you add to the list?" << endl;
    cin>>n;
    for( int i=1; i<=n; i++)
    {
        cout << endl << "Element "<< i << ".:" << endl;
        cout << "Enter x:";
        cin >>x;
        cout << "Enter y:";
        cin >> y;
        cout << "Enter z:";
        cin >> z;
        l.add_node(Point(x,y,z));
    }
}

void check_node(node *p) //check memory allocation
{
    if(p==NULL)
        throw "Space is not allocated!";

}

void caption() //function prints caption
{
    cout<<"*************************************"<<endl;
    cout<<"** Let's try how program works. :> **"<<endl;
    cout<<"*************************************"<< endl;
}



void check_how_it_works() //function shows how program works
{
    cout<<"** Operations on points **"<< endl;
    cout<<"**************************"<< endl;

    Point a = Point(1,2,3);
    Point b;
    add_point(b);
    cout<<"** Point entered by system (a) and by user (b).**"<< endl;
    cout<<"a->  "<<a<<"b->  "<<b;
    a+=b;
    cout<<"a+=b     "<<a<<endl;
    cout<<"a->  "<<a<<"b->  "<<b;
    a-=b;
    cout<<endl<<"a-=b       "<<a;
    cout<<"a->  "<<a<<"b->  "<<b;
    a*=b;
    cout<<endl<<"a*=b       "<<a;

    Point c;
    cout<<"a->  "<<a<<"b->    "<<b<< "c-> "<<c;
    c=a+b;
    cout<<endl<<"c=a+b      "<<c;
    c=a-b;
    cout<<endl<<"c=a-b      "<<c;
    c=a*b;
    cout<<endl<<"c=a*b      "<<c;


    cout <<"** Operations on lists**"<< endl;
    list l=list();
    add_list(l);
    cout << "-----  List l. - user adds points  ------" << endl;
    l.display_list();

    list k=list();
    k.add_node(a);
    k.add_node(b);
    k.add_node(c);
    cout << "------  List m. Points are added by system -----" << endl;
    k.display_list();

    k.add_node(Point(1,1,1));
    cout << "------  Addition Point (1,1,1) to our existing k list  -----" << endl;
    k.display_list();
    cout << "------  Addition k+=l on lists  -----" << endl;
    k+=l;
    k.display_list();
    cout << "------  Deleting first node in the row  -----" << endl;
    k.delete_node(1);
    k.display_list();
    cout << "------  Deleting node which does not exist in the row  -----" << endl;
    k.delete_node(1000);
    cout << "------  Setting new list as a result of addition lists k+m   -----" << endl;
    cout << "------  List k.  -----" << endl;
    k.display_list();
    cout << "------  List n=k+l  -----" << endl;
    list n;
    n=k+l;
    n.display_list();
    cout << "------  How many points we have in list (n)?  -----" << endl;
    int elements=n.elements_list();
    cout<< elements<<endl;
    cout << "------  Operator []  -----" << endl;
    cout << "n[1]" ;
    n[1].display_node();
    cout << "n[3]" ;
    n[3].display_node();
    cout << "n[10]" << endl;
    n[10].display_node();
}




